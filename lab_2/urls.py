from django.conf.urls import url
from django.conf.urls import include
import lab_1.urls as lab_1
from .views import index

#url for app, add your URL configuration

urlpatterns = [
	url(r'^lab-1/', include(lab_1, namespace='lab-1')),
    url(r'^$', index, name='index'),
]
