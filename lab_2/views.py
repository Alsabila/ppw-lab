from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#Create a content paragraph for your landing page:
landing_page_content = 'My short name is Alsa. I am 19 years old. It is nice to meet you.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)